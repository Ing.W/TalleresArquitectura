import { Component } from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import { Producto } from './models/producto';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'FormularioAngular';
  listaProducto: Producto[] =[];

  llenarLista: Producto = new Producto();

  adicionar(){
    this.listaProducto.push(this.llenarLista);
  }
}
