import { Component } from '@angular/core';
import {createObject} from "rxjs/internal/util/createObject";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  listaCompras ={
    item1: "papas",
    item2: "arroz",
    item3: "salchichas",
    item4: "ramen",
    item5: "frijoles",
    item6: "leche",
  }
  objectolista = Object.values(this.listaCompras);

}
